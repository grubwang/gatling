Gatling is an open-source load testing framework based on Scala, Akka and Netty.
Check http://gatling.io for details.

Basic idea:
1) start gatling recorder
2) set up your browser proxy so that it can record your browser interactions like click a button, fill in fields etc.
3) browse your target web site and do actions for one scenario
4) stop the recorder. It will generate Scala file that simulates the corresponding scenario
5) use gatling replay command to replay the recorded test scenario
6) gatling tool will issue multiple requests and measure min, max and avarage response time and generate a report.

To run the recorded scenarios targetting demo web site, use command:
./bin/gatling.sh

To run the recorded scenarios targeting grubmarket staging web site, use command:
(Note: -sf means "simulation file")
./bin/gatling.sh -sf user-files/gm/gm/
