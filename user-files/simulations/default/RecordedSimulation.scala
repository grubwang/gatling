package default

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class RecordedSimulation extends Simulation {

	val httpProtocol = http
		.baseURL("http://computer-database.gatling.io")
		.inferHtmlResources()

	val headers_0 = Map(
		"Accept" -> "*/*",
		"Accept-Encoding" -> "gzip, deflate",
		"Accept-Language" -> "en-us",
		"User-Agent" -> "ocspd/1.0.3")

	val headers_1 = Map(
		"Accept" -> "*/*",
		"X-Client-Data" -> "CKW2yQEIqbbJAQjEtskBCO6IygEI/ZXKAQi8mMoB")

	val headers_2 = Map(
		"Accept" -> "image/webp,image/*,*/*;q=0.8",
		"X-Client-Data" -> "CKW2yQEIqbbJAQjEtskBCO6IygEI/ZXKAQi8mMoB")

	val headers_3 = Map("Upgrade-Insecure-Requests" -> "1")

	val headers_4 = Map("Accept" -> "*/*")

	val headers_9 = Map(
		"Accept-Encoding" -> "gzip, deflate",
		"Origin" -> "http://computer-database.gatling.io",
		"Upgrade-Insecure-Requests" -> "1")

    val uri1 = "ocsp.digicert.com"
    val uri2 = "http://computer-database.gatling.io"
    val uri3 = "http://www.google.com/recaptcha/api"

	val scn = scenario("RecordedSimulation")
		.exec(http("request_0")
			.get("http://" + uri1 + "/MFYwVKADAgEAME0wSzBJMAkGBSsOAwIaBQAEFEn0vYoYv3YGmMXeQC1oO3Fq5OaGBBQ901Cl1qCt7vNKYApl0yHU%2BPjWDwIQDtU7wnsXD6lEWnaZI6vMCQ%3D%3D")
			.headers(headers_0))
		.pause(106)
		.exec(http("request_1")
			.get(uri3 + "/reload?c=03AHJ_VuuFE2oN6FQtPLyhFecTrHqVvmQkqrd6nnHErufcZ4mpF4wZJNeDsXv709GGEWbDrRg2HbKgLaCRVstV-L5KiegIvSC2f35VTRO6t59nHEfkNOCwTji_1fnCDAXhu4Hf73Zj2QlLfi54I30zS31l5kDH9oT-4RXFPvZbW3J6I4NXBrFZ3V9Sj68JjGqYJ9EiJxsNwlHp9U0IX9DgChJXBlzZpxkiYDHRJKT-XAGovyvQjMP6IWaOmpKeTiIXDNOFydT5HYmj9OffoWEga-B2qZYBwfXjAA&k=6LeI_LkSAAAAAKKj8A30rX0r1Hi4DEfqve2GYRgA&reason=t&type=image&lang=en&th=,8wKwJQOqqMuX_AS2FzcPyehgrxTwAAAAH6AAAAAG-ABq5mK5smtnZ_LdYCA4M-2zmOb3PkZZz6wautQIfJTqOIkAt25itEpHiYNNOvpEs8hErEM4LJoB7ubplUI6OM4ci1EkkO7UxDk9MutBwyfHRA1VNC94gO9IVaItqGcf6plV9jwVY8YP6-sPU9gAucGhd1CAP_g17eSCp1FdFWVMOYPnsDpcQEm3Cowzo5DwPvJVJ2SFMz-TQn2PmL1-f9MmOpfgnfgR1Wa5voJbDVgzI-ecqVsd1S7Q6h7aePfTUpJdkfXqD9sTdCMg8LTsxrFfI_BGWpL3UPDIAxfnghv427Ba7GzhbV7cOZ8OMuU-4eZpTSc4WM-fdJb4koZObnJPdCuNlf-spZkr5h8Tbb2BE3IxPQ3K08yCFUC6C1WmWmJ2-hPJTcez")
			.headers(headers_1)
			.resources(http("request_2")
			.get(uri3 + "/image?c=03AHJ_VutouJyY9kTSAOXbaNEiCwWH_E6pD5groUa3aDCPl5B55obLeTscHm6xg7D99Pz6DgZkHfZTFeaps3iYWau_mpCwnHaxKHfMeDTK3zKLntSlOycLTYHJlYlgFtEAkwcV3gdVfSdVChm2vUfRQAXqMKf0c-z__yur8aBucrtLsIvm8RhVFRmspQ1CNwyGkHZHsUZRig_wgl_ank2MqQJuaAPtoWegYtp2fbDX1qUjv_5WA1QhRl6396KiBXSbe5V5fAUWHAOwBiBwGlkiWNZiY2LfTaBqqg&th=,bp-wJQOqqMuX_AS2FzcPyehgrxTwAAAAH6AAAAAC-ABqNc64M9eXNwaPrpLWtW42DKzPioxrne2HEwrhJFQ4dKYjs1xTJSu5T3BQYbY9UtbngnQFeBXVtlDDLbWwd6DQw21rG5TLbzxugHBvb0YC2oMPkWjs169gMYIn8Yz9mQWOUi3OcUCGINJmE9gAucGhd1CAP_g17eSCp1FdFWVMOYPnsDpcQEm3Cowzo5DwPvJVJ2SFMz-TQn2PmL1-f9MmOpfgnfgR1Wa5voJbDVgzI-ecqVsd1S7Q6h7aePfTUpJdkfXqD9sTdCMg8LTsxrFfI_BGWpL3UPDIAxfnghv427Ba7GzhbV7cOZ8OMuU-4eZpTSc4WM-fdJb4koZObnJPdCuNlf-spZkr5h8Tbb2BE3IxPQ3K08yCFUC6DDumWmJ13tLW68TE")
			.headers(headers_2)))
		.pause(22)
		.exec(http("request_3")
			.get("/")
			.headers(headers_3)
			.resources(http("request_4")
			.get(uri2 + "/favicon.ico")
			.headers(headers_4)
			.check(status.is(404))))
		.pause(8)
		.exec(http("request_5")
			.get("/computers?f=macbook")
			.headers(headers_3))
		.pause(27)
		.exec(http("request_6")
			.get("/computers/6")
			.headers(headers_3))
		.pause(49)
		.exec(http("request_7")
			.get("/computers/273")
			.headers(headers_3))
		.pause(26)
		// Browse
		// Edit
		.exec(http("request_8")
			.get("/computers/new")
			.headers(headers_3))
		.pause(33)
		.exec(http("request_9")
			.post("/computers")
			.headers(headers_9)
			.formParam("name", "test computer")
			.formParam("introduced", "2015-11-18")
			.formParam("discontinued", "2016-08-08")
			.formParam("company", "2"))

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}