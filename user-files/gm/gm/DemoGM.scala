package gm

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class DemoGM extends Simulation {

	val httpProtocol = http
		.baseURL("http://staging.grubmarket.com")
		.inferHtmlResources()

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_1 = Map(
		"Accept" -> "*/*",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_4 = Map(
		"Accept" -> "*/*",
		"Origin" -> "http://staging.grubmarket.com")

	val headers_5 = Map("Accept" -> "*/*")

	val headers_21 = Map("X-Client-Data" -> "CKW2yQEIqbbJAQjEtskBCO6IygEI/ZXKAQi8mMoB")

	val headers_32 = Map(
		"Accept" -> "application/json",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_51 = Map("Accept" -> "text/css,*/*;q=0.1")

    val uri01 = "http://api.mixpanel.com/track"
    val uri02 = "http://us-u.openx.net/w/1.0/sd"
    val uri03 = "idsync.rlcdn.com"
    val uri04 = "web-pixel.dpclk.com"
    val uri05 = "http://staging.grubmarket.com"
    val uri06 = "http://pixel.mathtag.com/event/img"
    val uri07 = "http://www.google-analytics.com/collect"
    val uri08 = "www.google.com"
    val uri09 = "http://d.adroll.com"
    val uri10 = "http://secure.adnxs.com/seg"
    val uri11 = "http://googleads.g.doubleclick.net/pagead/viewthroughconversion/0"
    val uri12 = "http://x.bidswitch.net/sync"
    val uri13 = "http://ads.yahoo.com"
    val uri14 = "http://ib.adnxs.com"
    val uri15 = "http://tags.mediaforge.com"
    val uri16 = "cm.dpclk.com"

	val scn = scenario("DemoGM")
		.exec(http("request_0")
			.get("/")
			.headers(headers_0)
			.resources(http("request_1")
			.get(uri05 + "/api/item/favoriteItems?userId=8&_=1449716297274")
			.headers(headers_1),
            http("request_2")
			.get(uri05 + "/api/item/getcart?_=1449716297275")
			.headers(headers_1),
            http("request_3")
			.get(uri07 + "?v=1&_v=j40&a=2012044643&t=pageview&_s=1&dl=http%3A%2F%2Fstaging.grubmarket.com%2Fhome&ul=en-us&de=UTF-8&dt=GrubMarket%20%7C%20Wholesome%20Food%20at%20Wholesale%20Prices.%20Always%2020-50%25%20OFF!&sd=24-bit&sr=1920x1200&vp=1576x893&je=1&fl=19.0%20r0&_u=AACAAAIRI~&jid=&cid=1349754608.1448354763&tid=UA-47707293-1&z=310581904"),
            http("request_4")
			.get(uri01 + "/?data=eyJldmVudCI6ICJtcF9wYWdlX3ZpZXciLCJwcm9wZXJ0aWVzIjogeyIkb3MiOiAiTWFjIE9TIFgiLCIkYnJvd3NlciI6ICJDaHJvbWUiLCIkY3VycmVudF91cmwiOiAiaHR0cDovL3N0YWdpbmcuZ3J1Ym1hcmtldC5jb20vaG9tZSIsIiRicm93c2VyX3ZlcnNpb24iOiA0NSwiJHNjcmVlbl9oZWlnaHQiOiAxMjAwLCIkc2NyZWVuX3dpZHRoIjogMTkyMCwibXBfbGliIjogIndlYiIsIiRsaWJfdmVyc2lvbiI6ICIyLjcuMiIsImRpc3RpbmN0X2lkIjogIjE1MTM4YWJkMGVmMTc3LTA0MGUwOThmMi0xMDM4Njk1Mi0xM2M2ODAtMTUxMzhhYmQwZjA0MjYiLCIkaW5pdGlhbF9yZWZlcnJlciI6ICJodHRwOi8vc3RhZ2luZy5ncnVibWFya2V0LmNvbS92aWV3Y2FydCIsIiRpbml0aWFsX3JlZmVycmluZ19kb21haW4iOiAic3RhZ2luZy5ncnVibWFya2V0LmNvbSIsIlJlZ2lvbiI6ICJVUyIsIm1wX3BhZ2UiOiAiaHR0cDovL3N0YWdpbmcuZ3J1Ym1hcmtldC5jb20vaG9tZSIsIm1wX2Jyb3dzZXIiOiAiQ2hyb21lIiwibXBfcGxhdGZvcm0iOiAiTWFjIE9TIFgiLCJ0b2tlbiI6ICIzM2E1MGEzMjg5YTk3NDE4YTFlMmE0NWRiZjMzZjNjZCJ9fQ%3D%3D&ip=1&_=1449716297465")
			.headers(headers_4),
            http("request_5")
			.get(uri15 + "/js/4402")
			.headers(headers_5),
            http("request_6")
			.get(uri15 + "/js/4398")
			.headers(headers_5),
            http("request_7")
			.get(uri15 + "/js/4403")
			.headers(headers_5),
            http("request_8")
			.get(uri15 + "/pix/4402?type=pos&href=http%3A%2F%2Fstaging.grubmarket.com%2Fhome&uid1=880:2D245B84625311E58A17D4E851069995&uid3=2D245B84625311E58A17D4E851069995")
			.headers(headers_0),
            http("request_9")
			.get(uri15 + "/pix/4398?type=pos&href=http%3A%2F%2Fstaging.grubmarket.com%2Fhome&uid1=880:2D245B84625311E58A17D4E851069995&uid3=2D245B84625311E58A17D4E851069995")
			.headers(headers_0),
            http("request_10")
			.get(uri15 + "/pix/4403?type=pos&href=http%3A%2F%2Fstaging.grubmarket.com%2Fhome&uid1=880:2D245B84625311E58A17D4E851069995&uid3=2D245B84625311E58A17D4E851069995")
			.headers(headers_0),
            http("request_11")
			.get(uri05 + "/api/item/newhomepage?stateCity=US&sortType=desc&sortBy=updatedDate&offset=0&topSeller=true&_=1449716297276")
			.headers(headers_1),
            http("request_12")
			.get("http://" + uri16 + "/cm?network_id=mediaforge&network_uid=2D245B84625311E58A17D4E851069995"),
            http("request_13")
			.get(uri10 + "?add=3444907&t=2"),
            http("request_14")
			.get(uri10 + "?add=3444892&t=2"),
            http("request_15")
			.get(uri10 + "?add=3444885&t=2"),
            http("request_16")
			.get(uri06 + "?mt_id=838422&mt_adid=149554&v1=&v2=&v3=&s1=&s2=&s3="),
            http("request_17")
			.get(uri06 + "?mt_id=838417&mt_adid=149552&v1=&v2=&v3=&s1=&s2=&s3="),
            http("request_18")
			.get(uri06 + "?mt_id=838420&mt_adid=149553&v1=&v2=&v3=&s1=&s2=&s3="),
            http("request_19")
			.get("http://" + uri04 + "/web/2306939a-e9c9-40e4-a005-f4e272c03b14?app_id=2306939a-e9c9-40e4-a005-f4e272c03b14&auth=-1466890152&df_uid1=15138abf19f-832e743b-932b-4483-b944-038234e23e23&df_uid=undefined&click_id=undefined&api_version=1.1.0&event=visit&page_url=http%3A%2F%2Ftags.mediaforge.com%2Fpix%2F4398%3Ftype%3Dpos%26href%3Dhttp%253A%252F%252Fstaging.grubmarket.com%252Fhome%26uid1%3D880%3A2D245B84625311E58A17D4E851069995%26uid3%3D2D245B84625311E58A17D4E851069995&referrer_url=http%3A%2F%2Fstaging.grubmarket.com%2Fhome&ua=Mozilla%2F5.0%20(Macintosh%3B%20Intel%20Mac%20OS%20X%2010_10_5)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F45.0.2454.85%20Safari%2F537.36&sent_at=1449716297776")
			.check(status.is(304)),
            http("request_20")
			.get(uri09 + "/pixel/VEQWDIJDYFCIRCZ7RE2KQA/IJN6B73KI5DTHE63VSFUQF?pv=24488063924.945892&cookie=DZMRS5NNGFEGRJ2JQWDKWN%3A42%7CKRY3CMHU5NHLRJR22ZSUEF%3A27%7CDL66G2Y3GZGCLJAHJWHXPK%3A234%7CIJN6B73KI5DTHE63VSFUQF%3A318%7CVEQWDIJDYFCIRCZ7RE2KQA%3A318%7COS76MTO4RZACZHUSXP6MJT%3A15&adroll_s_ref=&keyw=")
			.headers(headers_5)
			.check(status.is(304)),
            http("request_21")
			.get(uri11 + "/?label=null&guid=ON&script=0&ord=2904384185094386.5")
			.headers(headers_21),
            http("request_22")
			.get(uri09 + "/cm/f/out"),
            http("request_23")
			.get(uri09 + "/cm/x/out"),
            http("request_24")
			.get(uri09 + "/cm/o/out"),
            http("request_25")
			.get(uri09 + "/cm/g/out?google_nid=adroll5"),
            http("request_26")
			.get(uri02 + "?id=537103138&val=8d9b14ffca00ecc502e785721e0670de"),
            http("request_27")
			.get(uri12 + "?dsp_id=44&user_id=OGQ5YjE0ZmZjYTAwZWNjNTAyZTc4NTcyMWUwNjcwZGU"),
            http("request_28")
			.get("http://" + uri03 + "/377928.gif?partner_uid=8d9b14ffca00ecc502e785721e0670de"),
            http("request_29")
			.get(uri14 + "/seg?add=3326945&t=2"),
            http("request_30")
			.get(uri13 + "/pixel?id=2498203&t=2&piggyback=http%3A%2F%2Fads.yahoo.com%2Fcms%2Fv1%3Fesig%3D1~bf4e7dc4546a90c08591652d78a230d3f2ef5733%26nwid%3D10001032567%26sigv%3D1")))
		.pause(2)
		.exec(http("request_31")
			.get("/images/thumb/testussfstore/15375706381_pear_nw.jpg")
			.resources(http("request_32")
			.get(uri05 + "/addOrder/1145/1")
			.headers(headers_32)))
		.pause(14)
		.exec(http("request_33")
			.get("/myaccount/settings")
			.headers(headers_0)
			.resources(http("request_34")
			.get(uri07 + "?v=1&_v=j40&a=1899462929&t=pageview&_s=1&dl=http%3A%2F%2Fstaging.grubmarket.com%2Fmyaccount%2Fsettings&ul=en-us&de=UTF-8&dt=GrubMarket%20%7C%20Wholesome%20Food%20at%20Wholesale%20Prices.%20Always%2020-50%25%20OFF!&sd=24-bit&sr=1920x1200&vp=1576x893&je=1&fl=19.0%20r0&_u=AACAAAIRI~&jid=&cid=1349754608.1448354763&tid=UA-47707293-1&z=299112053"),
            http("request_35")
			.get(uri01 + "/?data=eyJldmVudCI6ICJtcF9wYWdlX3ZpZXciLCJwcm9wZXJ0aWVzIjogeyIkb3MiOiAiTWFjIE9TIFgiLCIkYnJvd3NlciI6ICJDaHJvbWUiLCIkcmVmZXJyZXIiOiAiaHR0cDovL3N0YWdpbmcuZ3J1Ym1hcmtldC5jb20vaG9tZSIsIiRyZWZlcnJpbmdfZG9tYWluIjogInN0YWdpbmcuZ3J1Ym1hcmtldC5jb20iLCIkY3VycmVudF91cmwiOiAiaHR0cDovL3N0YWdpbmcuZ3J1Ym1hcmtldC5jb20vbXlhY2NvdW50L3NldHRpbmdzIiwiJGJyb3dzZXJfdmVyc2lvbiI6IDQ1LCIkc2NyZWVuX2hlaWdodCI6IDEyMDAsIiRzY3JlZW5fd2lkdGgiOiAxOTIwLCJtcF9saWIiOiAid2ViIiwiJGxpYl92ZXJzaW9uIjogIjIuNy4yIiwiZGlzdGluY3RfaWQiOiAiMTUxMzhhYmQwZWYxNzctMDQwZTA5OGYyLTEwMzg2OTUyLTEzYzY4MC0xNTEzOGFiZDBmMDQyNiIsIiRpbml0aWFsX3JlZmVycmVyIjogImh0dHA6Ly9zdGFnaW5nLmdydWJtYXJrZXQuY29tL3ZpZXdjYXJ0IiwiJGluaXRpYWxfcmVmZXJyaW5nX2RvbWFpbiI6ICJzdGFnaW5nLmdydWJtYXJrZXQuY29tIiwiUmVnaW9uIjogIlVTIiwibXBfcGFnZSI6ICJodHRwOi8vc3RhZ2luZy5ncnVibWFya2V0LmNvbS9teWFjY291bnQvc2V0dGluZ3MiLCJtcF9yZWZlcnJlciI6ICJodHRwOi8vc3RhZ2luZy5ncnVibWFya2V0LmNvbS9ob21lIiwibXBfYnJvd3NlciI6ICJDaHJvbWUiLCJtcF9wbGF0Zm9ybSI6ICJNYWMgT1MgWCIsInRva2VuIjogIjMzYTUwYTMyODlhOTc0MThhMWUyYTQ1ZGJmMzNmM2NkIn19&ip=1&_=1449716320827")
			.headers(headers_4),
            http("request_36")
			.get(uri05 + "/api/account/message/8/unread")
			.headers(headers_32),
            http("request_37")
			.get(uri05 + "/api/item/getcart?_=1449716320858")
			.headers(headers_1),
            http("request_38")
			.get(uri05 + "/api/account/user/8?_=1449716320857")
			.headers(headers_1),
            http("request_39")
			.get(uri05 + "/static/js/gm/partial/Partial.js")
			.headers(headers_5)
			.check(status.is(304)),
            http("request_40")
			.get(uri09 + "/pixel/VEQWDIJDYFCIRCZ7RE2KQA/IJN6B73KI5DTHE63VSFUQF?pv=41228462.9419446&cookie=OS76MTO4RZACZHUSXP6MJT%3A15%7CVEQWDIJDYFCIRCZ7RE2KQA%3A319%7CIJN6B73KI5DTHE63VSFUQF%3A319%7CDL66G2Y3GZGCLJAHJWHXPK%3A235%7CKRY3CMHU5NHLRJR22ZSUEF%3A27%7CDZMRS5NNGFEGRJ2JQWDKWN%3A42&adroll_s_ref=http%3A//staging.grubmarket.com/home&keyw=")
			.headers(headers_5),
            http("request_41")
			.get(uri09 + "/cm/b/out"),
            http("request_42")
			.get("http://" + uri08 + "/ads/user-lists/0/?label=null&script=0&random=1942870597")
			.headers(headers_21),
            http("request_43")
			.get(uri14 + "/seg?add=3160954&t=2"),
            http("request_44")
			.get(uri09 + "/cm/x/out"),
            http("request_45")
			.get(uri09 + "/cm/l/out"),
            http("request_46")
			.get(uri13 + "/pixel?id=2498203&t=2&piggyback=http%3A%2F%2Fads.yahoo.com%2Fcms%2Fv1%3Fesig%3D1~bf4e7dc4546a90c08591652d78a230d3f2ef5733%26nwid%3D10001032567%26sigv%3D1"),
            http("request_47")
			.get(uri13 + "/cms/v1?esig=1~bf4e7dc4546a90c08591652d78a230d3f2ef5733&nwid=10001032567&sigv=1"),
            http("request_48")
			.get(uri09 + "/cm/r/in?xid=2e1OTrkDcM8nGMosFj8mqWgs"),
            http("request_49")
			.get(uri12 + "?dsp_id=44&user_id=OGQ5YjE0ZmZjYTAwZWNjNTAyZTc4NTcyMWUwNjcwZGU")))
		.pause(2)
		.exec(http("request_50")
			.get("/myaccount/favorites")
			.headers(headers_0)
			.resources(http("request_51")
			.get(uri05 + "/static/css/screens/my-account-favorites.css")
			.headers(headers_51),
            http("request_52")
			.get(uri07 + "?v=1&_v=j40&a=1580735623&t=pageview&_s=1&dl=http%3A%2F%2Fstaging.grubmarket.com%2Fmyaccount%2Ffavorites&ul=en-us&de=UTF-8&dt=GrubMarket%20-%20My%20Favorites&sd=24-bit&sr=1920x1200&vp=1576x893&je=1&fl=19.0%20r0&_u=AACAAAIRI~&jid=&cid=1349754608.1448354763&tid=UA-47707293-1&z=527061863"),
            http("request_53")
			.get(uri01 + "/?data=eyJldmVudCI6ICJtcF9wYWdlX3ZpZXciLCJwcm9wZXJ0aWVzIjogeyIkb3MiOiAiTWFjIE9TIFgiLCIkYnJvd3NlciI6ICJDaHJvbWUiLCIkcmVmZXJyZXIiOiAiaHR0cDovL3N0YWdpbmcuZ3J1Ym1hcmtldC5jb20vbXlhY2NvdW50L3NldHRpbmdzIiwiJHJlZmVycmluZ19kb21haW4iOiAic3RhZ2luZy5ncnVibWFya2V0LmNvbSIsIiRjdXJyZW50X3VybCI6ICJodHRwOi8vc3RhZ2luZy5ncnVibWFya2V0LmNvbS9teWFjY291bnQvZmF2b3JpdGVzIiwiJGJyb3dzZXJfdmVyc2lvbiI6IDQ1LCIkc2NyZWVuX2hlaWdodCI6IDEyMDAsIiRzY3JlZW5fd2lkdGgiOiAxOTIwLCJtcF9saWIiOiAid2ViIiwiJGxpYl92ZXJzaW9uIjogIjIuNy4yIiwiZGlzdGluY3RfaWQiOiAiMTUxMzhhYmQwZWYxNzctMDQwZTA5OGYyLTEwMzg2OTUyLTEzYzY4MC0xNTEzOGFiZDBmMDQyNiIsIiRpbml0aWFsX3JlZmVycmVyIjogImh0dHA6Ly9zdGFnaW5nLmdydWJtYXJrZXQuY29tL3ZpZXdjYXJ0IiwiJGluaXRpYWxfcmVmZXJyaW5nX2RvbWFpbiI6ICJzdGFnaW5nLmdydWJtYXJrZXQuY29tIiwiUmVnaW9uIjogIlVTIiwibXBfcGFnZSI6ICJodHRwOi8vc3RhZ2luZy5ncnVibWFya2V0LmNvbS9teWFjY291bnQvZmF2b3JpdGVzIiwibXBfcmVmZXJyZXIiOiAiaHR0cDovL3N0YWdpbmcuZ3J1Ym1hcmtldC5jb20vbXlhY2NvdW50L3NldHRpbmdzIiwibXBfYnJvd3NlciI6ICJDaHJvbWUiLCJtcF9wbGF0Zm9ybSI6ICJNYWMgT1MgWCIsInRva2VuIjogIjMzYTUwYTMyODlhOTc0MThhMWUyYTQ1ZGJmMzNmM2NkIn19&ip=1&_=1449716325048")
			.headers(headers_4),
            http("request_54")
			.get(uri05 + "/api/item/getcart?_=1449716325062")
			.headers(headers_1),
            http("request_55")
			.get(uri05 + "/api/account/message/8/unread")
			.headers(headers_32),
            http("request_56")
			.get(uri05 + "/api/item/favoriteItems?userId=8")
			.headers(headers_1),
            http("request_57")
			.get(uri05 + "/api/item/favoriteStores?userId=8")
			.headers(headers_1),
            http("request_58")
			.get(uri05 + "/images/thumb/new/3477693511_gluten.jpg"),
            http("request_59")
			.get(uri05 + "/images/thumb/new/14941212141_meat.jpg"),
            http("request_60")
			.get(uri09 + "/pixel/VEQWDIJDYFCIRCZ7RE2KQA/IJN6B73KI5DTHE63VSFUQF?pv=4079332691.61731&cookie=DZMRS5NNGFEGRJ2JQWDKWN%3A42%7CKRY3CMHU5NHLRJR22ZSUEF%3A27%7CDL66G2Y3GZGCLJAHJWHXPK%3A236%7CIJN6B73KI5DTHE63VSFUQF%3A320%7CVEQWDIJDYFCIRCZ7RE2KQA%3A320%7COS76MTO4RZACZHUSXP6MJT%3A15&adroll_s_ref=http%3A//staging.grubmarket.com/myaccount/settings&keyw=")
			.headers(headers_5),
            http("request_61")
			.get(uri11 + "/?label=null&guid=ON&script=0&ord=5574062441010028")
			.headers(headers_21),
            http("request_62")
			.get(uri09 + "/cm/b/out"),
            http("request_63")
			.get(uri09 + "/cm/l/out"),
            http("request_64")
			.get(uri14 + "/pxj?bidder=172&seg=802787&action=setuid(%27OGQ5YjE0ZmZjYTAwZWNjNTAyZTc4NTcyMWUwNjcwZGU%27)"),
            http("request_65")
			.get(uri09 + "/cm/o/out"),
            http("request_66")
			.get(uri13 + "/pixel?id=2498203&t=2&piggyback=http%3A%2F%2Fads.yahoo.com%2Fcms%2Fv1%3Fesig%3D1~bf4e7dc4546a90c08591652d78a230d3f2ef5733%26nwid%3D10001032567%26sigv%3D1"),
            http("request_67")
			.get(uri13 + "/cms/v1?esig=1~bf4e7dc4546a90c08591652d78a230d3f2ef5733&nwid=10001032567&sigv=1"),
            http("request_68")
			.get(uri02 + "?id=537103138&val=8d9b14ffca00ecc502e785721e0670de"),
            http("request_69")
			.get(uri12 + "?dsp_id=44&user_id=OGQ5YjE0ZmZjYTAwZWNjNTAyZTc4NTcyMWUwNjcwZGU")))

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}